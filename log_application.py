from flask import Flask, request, jsonify, make_response
from flask_restful import Api, Resource, reqparse, abort
from functools import wraps
from bson import ObjectId
import json
import uuid
import pymongo
import datetime
import jwt

app = Flask(__name__)
api = Api(app)

# Connect to the database
dbconn = pymongo.MongoClient("mongodb://localhost:27017/")
db = dbconn['log_table']
db_extracted_data_collection = db["extracted"]

''' NOT SURE ABOUT UUID
class UUIDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, uuid.UUID):
            # if the obj is uuid, we simply return the value of uuid
            return obj.hex
        return json.JSONEncoder.default(self, obj)'''


class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        return json.JSONEncoder.default(self, obj)


# def token_required(f):
#     @wraps(f)
#     def decorated(*args, **kwargs):
#         token = None

#         if 'x-access-token' in request.headers:
#             token = request.headers['x-access-token']

#         if not token:
#             return jsonify({'message': 'Token is missing!'}), 401

#         try:
#             data = jwt.decode(token, app.config['SECRET_KEY'])
#             # current_user = User.query.filter_by(
#             #     public_id=data['public_id']).first()
#         except:
#             return jsonify({'message': 'Token is invalid!'}), 401

#         return f(current_user, *args, **kwargs)

#     return decorated


class Hub(Resource):
    # method_decorators = {'post': [token_required]}

    # GET endpoint.
    def get(self):
        pass

    # Validates the string, extracts the data and stores it in a database
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("extracted-string")
        parser.add_argument("medium")
        # parser.add_argument("id")
        args = parser.parse_args()

        log = open("log.txt", "a+")
        log.write("\n" + str(datetime.datetime.now()) + "\n")
        # log.write("ID is : "+ args["id"])
        log.write("status: request recieved\n")
        log.write("medium: " + args["medium"] + "\n")
        log.write("recieved data: " + args["extracted-string"] + "\n")

        extracted_data = validate(args["extracted-string"])

        hub_data = {
            "_id": uuid.uuid4(),
            "extracted-string": extracted_data
        }

        if args["medium"] == "ocr":
            hub_data["medium"] = "ocr"
        elif args["medium"] == "qr":
            hub_data["medium"] = "qr"
        elif args["medium"] == "barcode":
            hub_data["medium"] = "barcode"

        x = db_extracted_data_collection.insert_one(hub_data)
        if x.acknowledged == True:
            return json.dumps(hub_data, cls=UUIDEncoder), 201
        else:
            return "Error", 404

    # PUT functionality.
    def put(self, name):
        pass

    # DELETE functionality, will delete the data from the database
    def delete(self, name):
        pass


##
# Setting up the resource urls here
##
api.add_resource(Hub, "/mlhub/")

app.run(host="0.0.0.0", debug=True)
